import React from 'react';
import TodoItem from './Components/TodoItem';
import axios from 'axios'
import SignIn from './Components/SignIn';
import SignUp from './Components/SignUp'
import MainList from './Components/MainList'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      value: "",
      itemLeft: 0,
      mode: 'All',
      styleButton: [' ', ' ', ' '],
      modeAuth: 3
    }
  }


  componentWillMount = () => {
    if (localStorage.getItem('token'));
    this.setState({modeAuth: 0})
    this.getAllElementUser();
  }


  getAllElementUser = () => { //for get all element of data base
    const AuthStr = 'Bearer ' + localStorage.getItem('token');
    axios.get(`${process.env.REACT_APP_API_URL}/v1/tasks`, {'headers': {'Authorization': AuthStr}}).then(
      res => {
        this.setState({tasks: res.data});
      }).catch(error => console.log('failed to display all tasks', error));
  }


  itemLeftFunc() { //for calculations left items
    const {tasks} = this.state;
    let item;
    let count = 0;
    for (item of tasks) {
      if (item.status === true)
        count++;
    } return count;
  }


  handleClickClearCompleted = (e) => { //for clear compeled
    const {tasks} = this.state
    tasks.forEach((item) => {
      if (item.status) {
        const inx = item.id;
        const AuthStr = 'Bearer ' + localStorage.getItem('token');
        axios.delete(`${process.env.REACT_APP_API_URL}/v1/task/${inx}`, {'headers': {'Authorization': AuthStr}}).then(
          res => {
            console.log(res);
            this.getAllElementUser();
          }
        ).catch(error => console.log('There was an error sending your request, please try again'));
      }
    })
  }


  handleClickAll = (e) => { //for display all items
    if (JSON.stringify(this.state.styleButton) !== JSON.stringify(['filter-on', '', ''])) {
      this.setState({mode: "All", check: false, styleButton: ['filter-on', '', '']});
    }
  }


  handleClickCompleted = (e) => { //for display completed
    if (JSON.stringify(this.state.styleButton) !== JSON.stringify([' ', 'filter-on', ' '])) {
      this.setState({mode: 'Completed', check: true, styleButton: [' ', 'filter-on', ' ']});
    }
  }


  handleClickActive = (e) => { //for display active
    if (JSON.stringify(this.state.styleButton) !== JSON.stringify([' ', ' ', 'filter-on'])) {
      this.setState({check: true, mode: 'Active', styleButton: [' ', ' ', 'filter-on']});
    }
  }


  handleChangeItem = (inx) => { //for checked item
    const {tasks} = this.state;
    let item;
    for (item of tasks) {
      if (item.id === inx) {
        item.status = !item.status;
        let body = item;
        const AuthStr = 'Bearer ' + localStorage.getItem('token');
        axios.put(`${process.env.REACT_APP_API_URL}/v1/task`, body, {'headers': {'Authorization': AuthStr}}).then(
          res => {
            this.getAllElementUser();
          }
        ).catch(error => console.log('There was an error sending your request, please try again'));
      }
    }
  }


  handleRemove = (inx) => { //for remove selected item
    const AuthStr = 'Bearer ' + localStorage.getItem('token');
    axios.delete(`${process.env.REACT_APP_API_URL}/v1/task/${inx}`, {'headers': {'Authorization': AuthStr}}).then(
      res => {
        this.getAllElementUser();
      }
    ).catch(error => console.log('There was an error sending your request, please try again'));
  }


  handleSubmitEditApp = (Event, inx, tmpValue) => { //for Submit edit item
    Event.preventDefault();
    const {tasks} = this.state;
    let item;
    for (item of tasks) {
      if (item.id === inx) {
        item.text = tmpValue;
        item.isupdating = false;
        let body = item;
        const AuthStr = 'Bearer ' + localStorage.getItem('token');
        axios.put(`${process.env.REACT_APP_API_URL}/v1/task`, body, {'headers': {'Authorization': AuthStr}}).then(
          res => {
            this.getAllElementUser();
          }
        ).catch(error => console.log('There was an error sending your request, please try again'));
      }
    }
  }

  heandleClickForAllAdd = (e) => { // for select/unselect all items
    const {tasks} = this.state;
    let item;
    for (item of tasks) {
      item.status = !item.status;
      let body = item;
      const AuthStr = 'Bearer ' + localStorage.getItem('token');
      axios.put(`${process.env.REACT_APP_API_URL}/v1/task`, body, {'headers': {'Authorization': AuthStr}}).then(
        res => {
          console.log(res);
          this.getAllElementUser();
        }
      ).catch(error => console.log('There was an error sending your request, please try again'));
    }
  }

  handleChange = (e) => { //for change state value
    this.setState({value: e.target.value});
  }

  handleSumbit = async (e) => { //for submit and add new item
    e.preventDefault();
    if (this.state.value.trim()) {
      const data = {text: this.state.value}
      const AuthStr = 'Bearer ' + localStorage.getItem('token');
      axios.post(`${process.env.REACT_APP_API_URL}/v1/task`, data, {'headers': {'Authorization': AuthStr}}).then(
        res => {
          console.log(res);
          this.getAllElementUser();
        }
      ).catch(error => console.log(error));
    } this.setState({value: ""});
  }


  handleClickSignIn = (e) => {
    this.setState({modeAuth: 1});
  }

  handleClickSignUp = (e) => {
    this.setState({modeAuth: 2});
  }

  switchState = () => {
    this.setState({modeAuth: 0});
    this.getAllElementUser();
  }

  switchStateLogOut = () => {
    this.setState({modeAuth: 3});
    localStorage.removeItem('token');
    this.setState({tasks: []});
  }


  render() {
    const {tasks} = this.state
    // eslint-disable-next-line default-case
    switch (this.state.modeAuth) {
      case 0:
        return (
          <div className="container">  <button onClick={this.switchStateLogOut}>Logout</button>
            <div className="wrap">
              <h1 className="heading">todos</h1>
              <div className="mid">
                {tasks.length ?
                  <button className="all-check" onClick={this.heandleClickForAllAdd}>&or;</button> : <div className="block"></div>}
                <form className="input-form" onSubmit={this.handleSumbit}>
                  <input type="text" autoComplete="off" className="input-stroke" placeholder="What needs to be done?" value={this.state.value} onChange={this.handleChange} />
                </form>
              </div>
              {tasks.length ?
                <div>
                  <ul className="items">
                    {tasks.map((task) => {
                      if (this.state.mode === "All") {
                        return <TodoItem key={`${task.id}`} task={task} handleChangeItem={this.handleChangeItem} handleRemove={this.handleRemove}
                          handleSubmitEditApp={this.handleSubmitEditApp} />
                      }
                      if (this.state.mode === "Completed") {
                        if (task.status)
                          return <TodoItem key={`${task.id}`} handleEdit={this.handleEdit} task={task} handleChangeItem={this.handleChangeItem} handleRemove={this.handleRemove}
                            handleSubmitEditApp={this.handleSubmitEditApp} />
                      }
                      if (this.state.mode === "Active") {
                        if (!task.status)
                          return <TodoItem key={`${task.id}`} handleEdit={this.handleEdit} task={task} handleChangeItem={this.handleChangeItem} handleRemove={this.handleRemove}
                            handleSubmitEditApp={this.handleSubmitEditApp} />
                      }
                    })
                    }
                    <div className="foot-panel">
                      <p className="tasks-text"> {tasks.length - this.itemLeftFunc()} item left</p>
                      <div className="footer">
                        <button className={`filter ${this.state.styleButton[0]}`} onClick={this.handleClickAll}><p className="tasks-text">All</p></button>
                        <button className={`filter ${this.state.styleButton[2]}`} onClick={this.handleClickActive}><p className="tasks-text">Active</p></button>
                        <button className={`filter ${this.state.styleButton[1]}`} onClick={this.handleClickCompleted}><p className="tasks-text">Completed</p></button>
                      </div>
                      <button className="filter" onClick={this.handleClickClearCompleted}><p className="tasks-text">Clean completed</p></button>
                    </div>
                  </ul>
                  <div className="design-first">
                  </div>
                  <div className="design-second">
                  </div >
                </div> : ""}
            </div>
          </div >
        );
      case 1:
        return (
          <SignIn switchState={this.switchState} />
        )
      case 2:
        return (<SignUp switchState={this.switchState} />)
      case 3:
        return (<MainList handleClickSignIn={this.handleClickSignIn} handleClickSignUp={this.handleClickSignUp} />)
    }
  }
}

export default App;

