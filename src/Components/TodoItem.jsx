import React from 'react';

class TodoItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tsk: '',
      style: "delete-img",
      isupdating: false
    }
  };

  
  componentWillMount = () => {
    this.setState({tsk: this.props.task.text})
  };

  handleEdit = (e) => {
    this.setState({isupdating: true})
  };

  Edit = (e) => {
    this.setState({tsk: e.target.value})
  };

  handleSubmitEdit = (e, inx, tsk) => {
    e.preventDefault();
    this.props.handleSubmitEditApp(e, inx, tsk)
    this.setState({isupdating: false})
  };

  handleMouseMoveDelete = (inx) => {
    this.setState({style: "vision-delete-img"})
  };

  handleLeaveMoveDelete = () => {
    this.setState({style: "delete-img"})
  };

  textDecoration = () => {
    if (this.props.task.status)
      return ("label-check")
    else
      return ("label-no-check")
  };

  render() {
    const {task} = this.props;
    const {index, tsk, style, isupdating} = this.state;
    return (
      <div className="blur-delete" onMouseMove={() => this.handleMouseMoveDelete(index)} onMouseLeave={() => this.handleLeaveMoveDelete(index)}>
        <li className="edit-component" onDoubleClick={this.handleEdit}>
          <div className="check">
            {!isupdating && <div>
              <input type="checkbox" id={`${task.id}`} checked={task.status} onChange={() => this.props.handleChangeItem(task.id)} />
              <label className={this.textDecoration()} htmlFor={`${task.id}`}>{task.text}</label>
            </div>}
            {isupdating &&
              <form onSubmit={(e) => this.handleSubmitEdit(e, task.id, tsk)}>
                <input type="text" autoComplete="off" placeholder="Edit your task" className="edit-stroke" value={tsk} onChange={this.Edit} />
              </form>}
          </div>
          <button className="handle-remove" onClick={() => this.props.handleRemove(task.id)}> <img className={style} alt="" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiBoZWlnaHQ9IjUxMnB4IiB2aWV3Qm94PSIwIDAgMzQ0IDM0NC45NzEyOSIgd2lkdGg9IjUxMnB4Ij48cGF0aCBkPSJtMzQzLjA4MjAzMSAzMzAuNDM3NS0xNTguODA4NTkzLTE1Ny45NDUzMTIgMTU3Ljk3NjU2Mi0xNTguODQ3NjU3YzMuMTE3MTg4LTMuMTMyODEyIDMuMTA1NDY5LTguMTk5MjE5LS4wMjczNDQtMTEuMzE2NDA2LTMuMTMyODEyLTMuMTE3MTg3LTguMTk5MjE4LTMuMTAxNTYzLTExLjMxNjQwNi4wMzEyNWwtMTU3Ljk0MTQwNiAxNTguODI0MjE5LTE1OC44MzIwMzItMTU3Ljk3NjU2M2MtMy4xMzI4MTItMy4xMTcxODcyLTguMTk5MjE4LTMuMTA1NDY5LTExLjMxNjQwNi4wMjczNDQtMy4xMTcxODcgMy4xMzI4MTMtMy4xMDU0NjggOC4xOTkyMTkuMDI3MzQ0IDExLjMxNjQwNmwxNTguODA4NTk0IDE1Ny45NDE0MDctMTU3Ljk2MDkzOCAxNTguODMyMDMxYy0zLjExNzE4NyAzLjEzMjgxMi0zLjEwNTQ2OCA4LjE5OTIxOS4wMjczNDQgMTEuMzE2NDA2czguMTk5MjE5IDMuMTA1NDY5IDExLjMxNjQwNi0uMDI3MzQ0bDE1Ny45Mjk2ODgtMTU4LjgwODU5MyAxNTguNzk2ODc1IDE1Ny45NzY1NjJjMy4xMzI4MTIgMy4xMTcxODggOC4xOTkyMTkgMy4xMDU0NjkgMTEuMzE2NDA2LS4wMjczNDQgMy4xMTcxODctMy4xMzI4MTIgMy4xMDU0NjktOC4xOTkyMTgtLjAyNzM0NC0xMS4zMTY0MDZ6bTAgMCIgZmlsbD0iIzFlODFjZSIvPjwvc3ZnPgo=" /> </button>
        </li>
      </div>
    )
  }
}

export default TodoItem;


