import React from 'react';
import axios from 'axios'

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      first_name: '',
      last_name: '',
      e_mail: '',
      password: ''
    }
  }

  heandleChangeSignUpFirstName = (e) => {
    this.setState({first_name: e.target.value})
  };
  heandleChangeSignUpLastName = (e) => {
    this.setState({last_name: e.target.value})
  }
  heandleChangeSignUpEmail = (e) => {
    this.setState({e_mail: e.target.value})
  }
  heandleChangeSignUpPassword = (e) => {
    this.setState({password: e.target.value})
  }
  handleClickSignUp = async (e) => {
    e.preventDefault();
    const data = this.state;
    axios.post(`${process.env.REACT_APP_API_URL}/v1/signUp`, data).then(
      res => {
        localStorage.setItem('token', res.data)
        this.props.switchState();
      }
    ).catch(error => console.log(error));
  }

  render() {
    return (
      <>
        <div className="bg">
          <div className="display">
            <div className="sign-up">
              <h1>SignUp</h1>
              <form className="form" >
                <div>
                  <p>First Name:</p>
                  <input type="text" value={this.state.first_name} onChange={this.heandleChangeSignUpFirstName}
                    className="first-name" />
                  <p>Last name:</p>
                  <input type="text" value={this.state.last_name}
                    onChange={this.heandleChangeSignUpLastName} className="first-name" />
                  <p>e-mail:</p>
                  <input type="text" value={this.state.e_mail}
                    onChange={this.heandleChangeSignUpEmail} className="first-name" />
                  <p>Password:</p>
                  <input type="text" value={this.state.password}
                    onChange={this.heandleChangeSignUpPassword} className="first-name" />
                </div>
                <button onClick={this.handleClickSignUp}>sucessful</button>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  };
}



export default SignUp;